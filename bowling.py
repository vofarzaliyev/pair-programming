from random import randint as r

class bowling_game():
    def __init__(self):
        self.frame = 1
        self.first = 0
        self.strike = False
        self.spare = False
        self.score = 0
        self.pins_up = 10
        self.bowl()

    def bowl(self):
        while self.frame <= 10:
            print("Frame:", self.frame)
            self.pins_up = 10
            self.pins_up -= r(0,self.pins_up)
            self.first = self.pins_up
            if self.isStrike():
                print("STRIKE!!")
                self.strike = True
                self.score += 10
            else:
                self.pins_up -= r(0,self.pins_up)
                if self.isSpare():
                    print("SPARE!!")
                    self.spare = True
                    self.score += 10
                else:
                    self.scoring()
            if self.frame == 10:
                if self.isStrike() or self.isSpare():
                    self.pins_up = self.pins_up - r(0,10)
                    self.score += 10-self.pins_up
                    self.scoring()
            self.frame += 1
        print('Your final score is',self.score,'congratulations!!')

    def scoring(self):
        if self.strike == True:
            self.score += 2*(10-self.pins_up)
        elif self.spare == True:
            self.score += self.first*2 + (10-self.pins_up)
        else:
            self.score += (10-self.pins_up)
        self.strike = False
        self.spare = False

    def isStrike(self):
        return True if self.pins_up == 0 else False

    def isSpare(self):
        return True if self.pins_up == 0 else False

if __name__ == '__main__':
    bowling_game()
