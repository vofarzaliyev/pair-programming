import numpy as np
import time

class game_of_life():
    def __init__(self, n):
        self.n = n
        self.current = np.zeros((n, n), dtype = bool)
        self.generation = 1
        self.next = np.zeros((n, n), dtype = bool)
        self.neighbor = np.zeros((n,n))

    def get_current(self):
        return self.current

    def initial(self, x, y):
        self.current[x, y] = True

    def finding_neighbor(self, x, y):
        neighbor_lst = [self.current[(x+1)%self.n, y],self.current[(x+1)%self.n, (y-1)%self.n],self.current[x, (y-1)%self.n],self.current[(x-1)%self.n, (y-1)%self.n],self.current[(x-1)%self.n, y],self.current[(x-1)%self.n, (y+1)%self.n],self.current[x, (y+1)%self.n],self.current[(x+1)%self.n, (y+1)%self.n]]
        return neighbor_lst

    def count_neighbor(self):
        self.neighbor = np.zeros((self.n, self.n))
        for i in range(self.n):
            for j in range(self.n):
                count = 0
                for item in self.finding_neighbor(i, j):
                    if item == True:
                        count += 1
                self.neighbor[i, j] = count

    def next_gen(self):
        print(self.current)
        for i, row in enumerate(self.neighbor):
            for j, cell in enumerate(row):
                if self.current[i, j]:
                    if cell == 3 or cell == 2:
                        self.next[i, j] = True
                    elif cell < 2:
                        self.next[i, j] = False
                    elif cell > 3:
                        self.next[i, j] = False
                else:
                    if cell == 3:
                        self.next[i, j] = True
        self.current = self.next
        print("End of generation",self.generation)
        self.generation += 1

    def dead(self):
        for row in self.current:
            for cell in row:
                if cell == True:
                    return True
        return False

if __name__ == '__main__':
    game = game_of_life(4)
    #initial_x = int(input('Initial x of live cells:'))%5
    #initial_y = int(input('Initial y of live cells:'))%5
    game.initial(2, 1)
    game.initial(2, 2)
    game.initial(2, 3)


    while game.dead():
        game.count_neighbor()
        game.next_gen()
        time.sleep(5)
