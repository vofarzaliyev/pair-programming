class vending_machine():
    def __init__(self):
        self.credit = 0
        self.invalid_coin_return = ''
        self.coin_return = 0
        self.valid_coins = {'nickel':0.05,'dime':0.10, 'quarter':0.25, 'dollar':1.0}
        self.valid_products = {'cola':1.0, 'crisps':0.50,'chocolate':0.65}
        self.running()
    def insert_coin(self, coin):
        if self.check_coin(coin):
            self.credit +=  self.valid_coins[coin]
        else:
            self.invalid_coin_return += coin + ', '
            print("The invalid coin return contains: ",self.invalid_coin_return)
    def check_coin(self, coin):
        if coin in self.valid_coins:
            return True
    def dispense_product(self, product):
        if product in self.valid_products and self.check_credit(product):
            self.credit -= self.valid_products[product]
            print("Enjoy your %s!"%product)
            print("Money returned is $ %s"%self.return_change())
        elif not self.check_credit(product):
            print('Sorry, the price of', product, 'is $', self.valid_products[product])
    def return_change(self):
        change = self.credit
        for key in reversed(sorted(self.valid_coins.values())):
            while self.credit - key >= 0:
                self.credit -= key
                self.coin_return += key
        return self.coin_return
    def check_credit(self, product):
        if self.valid_products[product] <= self.credit:
            return True
    def check_product(self,product):
        if product in self.valid_products:
            return True
    def get_credit(self):
        return self.credit
    def get_return(self):
        self.return_change()
        return self.coin_return
    def running(self):
        while True:
            customer = input('CREDIT: $ %s INSERT COIN:  '%round(self.get_credit(),2))
            if self.check_product(customer):
                self.dispense_product(customer)
            else:
                self.insert_coin(customer)
v = vending_machine()
